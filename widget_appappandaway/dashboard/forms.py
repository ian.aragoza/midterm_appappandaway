from django import forms
from .models import WidgetUser
    
    
class UserForm(forms.ModelForm):
    class Meta:
        model = WidgetUser
        fields = '__all__'

from django.shortcuts import render
from django.http import HttpResponse
from django.http.response import HttpResponseRedirect
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView

from .models import WidgetUser
from .forms import UserForm


# Create your views here.
def dashboard(request):
    users   = WidgetUser.objects.all()
    context = {
        'object_list': users
    }   
    return render(request, 'dashboard/dashboard.html', context)


class WidgetUserDetailView(DetailView):
    model = WidgetUser
    template_name = 'dashboard/widgetuser-details.html'


class WidgetUserCreateView(CreateView):
    model = WidgetUser
    fields = '__all__'
    template_name = 'dashboard/widgetuser-add.html'
    
    def post(self, request, *args, **kwargs):
        form = UserForm(request.POST)
        if form.is_valid():
            new_user = form.save()
            redirect_link = "../" + new_user.get_absolute_url() + "/details/"
            return HttpResponseRedirect(redirect_link)

        else:
            return render(request, self.template_name, {'form': form})


class WidgetUserUpdateView(UpdateView):
    model = WidgetUser
    fields = '__all__'
    template_name = 'dashboard/widgetuser-edit.html'
    
    success_url = "../details/"



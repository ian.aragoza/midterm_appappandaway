from django.contrib import admin
from .models import Department, WidgetUser


class DepartmentAdmin(admin.ModelAdmin):
    model = Department
    list_display = ('dept_name', 'home_unit',)
    search_fields = ('dept_name',)
    list_filter = ('home_unit',)


class UserAdmin(admin.ModelAdmin):
    model = WidgetUser
    list_display = ('last_name', 'first_name', 'middle_name', 'department',)
    search_fields = ('last_name', 'first_name', 'middle_name',)
    list_filter = ('department',)

    fieldsets = [
        ('Name', {
            'fields': [
                ('last_name', 'first_name', 'middle_name'), 'department'
            ]
        }),
    ]


admin.site.register(Department, DepartmentAdmin)
admin.site.register(WidgetUser, UserAdmin)

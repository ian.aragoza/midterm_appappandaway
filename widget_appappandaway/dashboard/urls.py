from django.shortcuts import render
from django.urls import path
from .views import dashboard, WidgetUserDetailView, WidgetUserCreateView, WidgetUserUpdateView

# Create your views here.
urlpatterns = [
    path('dashboard/', dashboard, name='dashboard'),
    path('widgetusers/<int:pk>/details/', WidgetUserDetailView.as_view(), name='user-details'),
    path('widgetusers/add/', WidgetUserCreateView.as_view(), name='add-user'),
    path('widgetusers/<int:pk>/edit/', WidgetUserUpdateView.as_view(), name='edit-user'),
]

app_name = "dashboard"
